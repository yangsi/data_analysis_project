#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# In[3]:


# categorical feature -- categorical lable 
def cat_vs_label(df, feat, y):
    count_df= df.groupby([feat, y])["id"].count().unstack()
    print(count_df)
    print(" ")
    print("-" * 7)
    print("纵向成分统计")
    tmp_df = count_df.div(count_df.sum(axis = 0), axis = 1)
    tmp_df = tmp_df.sort_values( by = [4, 3, 2, 1], ascending = [False]*4)
    print(tmp_df)
    print(" ")
    print("-" * 7)
    print("横向成分统计")
    tmp_df = count_df.div(count_df.sum(axis = 1), axis = 0)
    tmp_df = tmp_df.sort_values( by = [4, 3, 2, 1], ascending = [False]*4)
    print(tmp_df)

