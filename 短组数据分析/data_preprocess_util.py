#!/usr/bin/env python
# coding: utf-8

# In[2]:


import os
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# In[3]:


# 将百分比字符串，如“100%” 转换为float
def str2percent(s):
    if pd.isnull(s):
        return s
    elif s[-1] == "%":
        return int(s[:-1])/100
    else:
        print("Unexpected value catched ", s)
        return s


# In[4]:


# 将字符 “t/f” 转换为布尔量 True/False
def str2boolean(ch):
    if ch == "t":
        return True
    elif ch == "f":
        return False
    elif df.isnull(ch):
        return ch
    else:
        print("Unexpected value catched ", ch)
        return ch


# In[5]:


# 将字符串如 “$500” 提取树值
def str2price(string):
    if pd.isnull(string):
        return string
    elif string[0] == "$":
        return float(string[1:].replace(",", ""))
    else:
        print("unexpected value catched ", string)
        return string


# In[6]:


# 处理邮政编码，将可以整数化的邮政编码变为整数形式
def str2int(x):
    if pd.isnull(x):
        return x
    try:
        if isinstance (x, float):
            return int(x)
        elif isinstance(x, str):
            x = x.replace(" ", "")
            x = x.split(".")[0]
            return int(x)
    except:
        print("Unexpected value catched ", x)
        return np.nan


# In[7]:


# 将calendar update 字符串形式量化为具体数值
# 其中有比如 “never”， “today”， “one week ago”
def update2int(up_str):
    if up_str == "never":
        return -1
    elif up_str == "today":
        return 0
    elif up_str == "yesterday":
        return 1
    
    up = up_str.split(" ")
    if "day" in up[1] :
        unit = 1
    elif "week" in up[1]:
        unit = 7
    elif "month" in up[1]:
        unit = 30
    else:
        print("unexpected value catched ", up)
        return up_str
    if up[0] == "a" or up[0] == "one":
        freq = 1
    else:
        freq = int(up[0])
    
    return freq*unit


# In[10]:


# 处理 host_verifications
# 某列column的元素为list，list并集元素有限N个
# 创建N列，若每行具备则置1，否则置0
def list2onehot(listings_detail, col):
    listings_detail[col] = listings_detail[col].map(lambda v: eval(v))
    alist = []
    for l in listings_detail[col].values:
        alist.extend(l)
    alist = list(set(alist))
    print("There are possible values in the list ", alist)
    
    for item in alist:
        listings_detail[item] = listings_detail[col].apply(lambda d: 1 if item in d else 0) 
    
    return listings_detail


# In[9]:


# 若缺失值大于threshold，则该列视为无用列，则舍去
def drop_useless_cols(listings_detail, threshold = 0.99):
    # 查看所有值均为nan的column，或者大部分值均为nan的column，则该为无用column
    
    count = listings_detail.isna().sum()

    listing_size = listings_detail.shape[0]
    useless_col = []
    for key,value in count.items():
        if value == 0:
            del count[key]
        if value >= listing_size * threshold:
            useless_col.append(key)
    
    print("useless columns found ", useless_col)
    listings_detail = listings_detail.drop(columns = useless_col, axis = 1)
    
    return listings_detail

#  关于处理 amenities 列
def str2list(string):
    if pd.isnull(string) or string is None:
        return string
    string = string.replace("{", "").replace("}", "")
    string = string.replace("\"", "").replace("\'", "")
    try:
        alist = string.split(",")
        return alist
    except:
        print("Catch unexpected string as fllowing!")
        print(string)
        return string
